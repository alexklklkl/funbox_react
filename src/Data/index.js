/*
* Тестовые данные в data.json
* Суффиксы:
* _s - selected
* _h - hover
*/

/*
* Возвращаем асинхронные данные в get()
*/
let Data = {
    get: () => {
        return new Promise(resolve =>
            fetch('data.json')
                .then(response =>
                    resolve(response.json())
                )
        );
    },
};

export default Data;