import './styles.scss';
import Settings from '../../settings';
import React from 'react';

function Header() {
    return (
        <div className="row">
            <div className="col-full">
                <h1 className="title">
                    {Settings.title}
                </h1>
            </div>
        </div>
    );
}

export default Header;