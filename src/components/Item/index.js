import './styles.scss';
import React from 'react';
import Brand from '../ItemComponents/Brand';
import Comments from '../ItemComponents/Comments';
import Image from '../ItemComponents/Image';
import Description from '../ItemComponents/Description';

class Item extends React.Component {
    constructor(props) {
        super(props);
        /* Не нужно, используем стрелочные функции */
        //this.handle... = this.handle....bind(this);
        this.state = {
            /* Варианты состояния: enabled, selected, disabled, по умолчание из item */
            class: this.props.item.class,
            /* есть ли hover */
            hover: false,
        };
    }

    getClass = () => ' ' + this.state.class;

    getHover = () => ' ' + (this.state.hover ? 'hover' : '');

    /* Если элемент не disabled, вернем обработчик */
    getClickHandler = () => this.state.class !== 'disabled' ? this.handleClick : null;

    handleClick = (e) => {
        e.preventDefault();
        let state = {};
        if (this.state.class === 'enabled') {
            state.class = 'selected';
        } else if (this.state.class === 'selected') {
            state.class = 'enabled';
        }
        /* При "выборе" элемента убрать hover, вернуть в следующем onMouseEnter */
        state.hover = false;
        this.setState(state);
    };

    handleMouseEnter = () => this.setState({hover: true});

    handleMouseLeave = () => this.setState({hover: false});

    /*
    * Brand и Comments вынесем в дочерние компоненты - особое поведение
    * Image и Description тоже - упростим компонент
    * В props можно передавать не весь item, а нужные свойства
    */
    render() {
        /* class и hover - из state, item - из props */
        const className = this.state.class;
        const hover = this.state.hover;
        const item = this.props.item;

        return (
            <div
                className={"item" + this.getClass() + this.getHover()}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
            >
                <div className="top-container" onClick={this.getClickHandler()}>
                    <Image item={item} />
                    <div className="inner-frame" />
                    <div className="inner-text">
                        <Brand item={item} class={className} hover={hover} />
                        <div className="title">{item.title}</div>
                        <div className="sub_title">{item.sub_title}</div>
                        <Description item={item} />
                    </div>
                    <div className="weight">
                        <div className="value">{item.value}</div>
                        <div className="units">кг</div>
                    </div>
                </div>
                <div className="bottom-container">
                    <Comments item={item} class={className} click={this.getClickHandler()} />
                </div>
            </div>
        );
    }
}

export default Item;