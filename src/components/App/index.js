import './styles.scss';
import React from 'react';
import Data from '../../Data';
import Header from '../Header';
import Items from '../Items';

class App extends React.Component {
    /*
    * Не хорошо хранить данные в state, но для примера асихронного получения - можно
    */
    state = {
        items: [],
    };

    componentDidMount() {
        this.init();
    }

    init = () =>
        this.requestData()
            .then(this.verifyData)
            .then(this.setData)
            .catch(this.error);

    /*
    * Получим данные, ожидаем json
    */
    requestData = () => Data.get();

    /*
    * TODO - для тестового задания считаем, что данные достоверны
    */
    verifyData = data => data;

    setData = data => this.setState({items: data});

    error = err => console.log(err);

    render() {
        return (
            <div className="content_container">
                <div className="content">
                    <div className="container">
                        <Header />
                        <Items items={this.state.items} />
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
