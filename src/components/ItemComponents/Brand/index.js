import React from 'react';

function Brand(props) {
    return (
        <div className="brand">
            {(props.class === 'selected' && props.hover) ? props.item.brand_s_h : props.item.brand}
        </div>
    );
}

export default Brand;