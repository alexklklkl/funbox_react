import React from 'react';

function Comments(props) {
    return (
        <div className="comments">
            {/* Основной текст comments */}
            {props.class === 'selected' ? props.item.comments_s : props.item.comments}
            {/* ссылка "купи" */}
            {props.class === 'enabled'
                ? <span>, <a href="/" onClick={props.click}>купи</a></span>
                : null
            }
        </div>

    );
}

export default Comments;