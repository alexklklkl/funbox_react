import React from 'react';

function Description(props) {
    return (
        <div className="description" dangerouslySetInnerHTML={{__html: props.item.description}} />
    );
}

export default Description;