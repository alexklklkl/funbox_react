import React from 'react';

function Image(props) {
    return (
        <img className="inner-image" src={`images/${props.item.image}.png`} alt={props.item.title} />
    );
}

export default Image;