import React from 'react';
import Item from '../Item';

class Items extends React.Component {
    render() {
        return (
            <div className="row">
                {this.props.items.map((item) =>
                    /* Обернем .item в .col (для верстки) */
                    <div key={item.id} className="col">
                        <Item item={item} />
                    </div>
                )}
            </div>
        );
    }
}

export default Items;